module.exports = {
  HTML: function (title, body) {
  return `
  <!DOCTYPE html>
  <head>
      <title>${title}</title>
  </head>
      <body>
          <canvas id="tetris" width="200px" height="400px" style="border-color: black; border-width:1em;"></canvas>
          <p>${body}</p>
          <div width="200px" height="400px" style="border-color: black; border-width:1em;"></div>
      </body>
  
      <script>console.log('this is test')</script>
  </html>
      `;
  }
}