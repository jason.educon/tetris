const express = require('express')
const app = express()
const fs = require('fs');
const template = require('./lib/template');

app.get('/', (request,response) => {
    response.sendFile(__dirname + '/index.html')
})

app.listen(3000);